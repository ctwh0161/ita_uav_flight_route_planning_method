 # coding:utf-8
import random as rnd
import matplotlib.pyplot as plt
import math
import copy
import numpy as np
from numpy import random
import sys
import time

# 標準入力よりデータを読み込む
def read_data():
    buff = []
    for a in sys.stdin:
        b = a.split()
        buff.append((int(b[0]), int(b[1])))
    return buff

# 距離の計算
def distance(ps):
    size = len(ps)
    secset1 = int(size)
    table = [[0] * size for _ in range(size)]
    for i in range(secset1):
        for j in range(secset1):
            if i != j:
                dx = ps[i][0] - ps[j][0]
                dy = ps[i][1] - ps[j][1]
                table[i][j] = math.sqrt(dx * dx + dy * dy)
    return table
    
# 2-opt 法
def opt_2(size, path,route):
    global distance_table
    total = 0
    while True:
        count = 0
        for i in range(size - 2):
            i1 = i + 1
            for j in range(i + 2, size):
                if j == size - 1:
                    j1 = 0
                else:
                    j1 = j + 1
                    
                if j1 == size:
                    j1 = 0
                if i1 == size:
                    i1 = 0
                    
                if i != 0 or j1 != 0:
                    l1 = path[route[i]-1][route[i1]-1]
                    l2 = path[route[j]-1][route[j1]-1]
                    l3 = path[route[i]-1][route[j]-1]
                    l4 = path[route[i1]-1][route[j1]-1]
                    if l1 + l2 > l3 + l4:
                        # つなぎかえる
                        new_route = copy.deepcopy(route[i1:j+1])
                        route[i1:j+1] = new_route[::-1]
                        count += 1
            total += count
        if count == 0 or total >= 10000: break
    return path, total,route
    
# 経路の長さ
def path_length(path):
    global distance_table
    n = 0
    i = 1
    for i in range(1, len(path)):
        n += distance_table[path[i - 1]][path[i]]
    n += distance_table[path[0]][path[-1]]
    return n

### Nearest Neighbor 法
###
def greedy0(point_table):
    global distance_table
    size = len(point_table)
    secset1 = int(size)
    path = [[0]]*secset1
    pathnum = [0]*secset1
    reach = [[0]*secset1]*secset1

    for i in range(size):
        for j in range(size):
            #各地点間の距離を計算
            reach[i][j] = np.linalg.norm(np.array(point_table[i]) - np.array(point_table[j]))
        path[i] = copy.deepcopy(reach[i])
    #print(path)
    
    #移動順を構築する
    pathnum[0] = 1
    #max = 10000
    for i in range(size):
        for j in range(size):
            max = 10000
            ok = 0
            #if i==0:
                #print(pathnum)
            #pathnum[i]に次のデバイス番号を入力
            if pathnum[j] == 0 and pathnum[j-1] != 0 :
                ok = 0
            
            #ここから先がうまくいってない
                for k in range(size):
                    #すでに入力されているデバイス番号がないことを確認
                    #入力されていれば弾く (できてない)
                    if pathnum[k] == k+1 and k != 0:
                        ok = 1
                    
                for k in range(size):
                    if path[pathnum[j-1]-1][k] < max and path[pathnum[j-1]-1][k] != 0:
                        ok = 0
                        for m in range(size):
                        #すでに入力されているデバイス番号がないことを確認
                        #入力されていれば弾く (できてない)
                            if pathnum[m] == k+1 and m != 0:
                                ok = 1
                            if k+1 == 1:
                                ok = 1
                            
                        if ok == 0:
                            max = path[pathnum[j-1]-1][k]
                            pathnum[j] = k+1
    return path,pathnum

##ここから上は2-opt法


def device_search(AoI_coordinate,AoI_num,group,secset1,secset2):

    num = [0]*len(AoI_coordinate)
    num_coordinate = [0]*len(AoI_coordinate)
    #secset1 = int(len(AoI_coordinate)/2)
    #secset2 = int(len(AoI_coordinate) - len(AoI_coordinate)/2)
    #min_device_number = [0]*(len(AoI_coordinate)/2)

    min_device_number = [0]*secset1
    print(min_device_number)
    
    #デバイスを２つのグループに分ける
    deviceID1 = 1
    deviceID2 = 1
    for i in range(len(AoI_coordinate)):
        if i < secset1 :
            num[i] = AoI_num[i]
            num_coordinate[i] = [AoI_coordinate[i][0],AoI_coordinate[i][1],AoI_num[i],0]
        else:
            num[i] = AoI_num[i]
            num_coordinate[i] = [AoI_coordinate[i][0],AoI_coordinate[i][1],AoI_num[i],1]
    num.sort()

    #num_coordinate[i][0]はデバイスX座標,num_coordinate[i][1]はデバイスY座標,num_coordinate[i][2]はAoI更新頻度,num_coordinate[i][3]は デバイスグループ番号
    for i in range(len(AoI_coordinate)):
        for j in range(len(AoI_coordinate)):
            if num[i] == num_coordinate[j][2]:
                keep = num_coordinate[j]
                keep2 = num_coordinate[i]
                num_coordinate[i] = keep
                num_coordinate[j] = keep2
                
    for i in range(secset1):
        min_device_number[i] = i+1
                              
    return num_coordinate,min_device_number

def AoInum(AoI,move):

    ans = 0
    start = 0
    end = 0
    up = 0
    down = 0
    
    for i in range(len(AoI)):
    #print(ans)
        if i == 0:
            start = 0
            continue
    #三角形の面積 初めの箇所の面積とその後
        if  AoI[i] == 0 and AoI[i] < AoI[i-1] and up == 0 :
            end = i
            ans = ans + ((AoI[i-1] + 1)*(end - start)/2)
            if ((AoI[i])*(end - start)) % 2 == 1 :
                ans = ans + 0.5
            start = i
            continue
            
    #台形の面積 前の三角形の面積と上面の記録
        if AoI[i] != 0 and AoI[i] < AoI[i-1] and up == 0:
            end = i
            ans = ans + ((AoI[i-1] + 1)*(end - start)/2)
        
            #切り捨てられる小数を補填
            if ((AoI[i])*(end - start)) % 2 == 1 :
                ans = ans + 0.5
            
            start = i
            up = AoI[i]
            #print("B")
            continue
            
    #台形の面積　下面の記録と面積 上面が記録されたのちに動作
        if AoI[i] < AoI[i-1] and up != 0:
            down = AoI[i-1]
            end = i
            ans = ans + ((up+down)*(end - start)/2)
        
            if ((up+down)*(end - start)) % 2 == 1 :
                ans = ans + 0.5
            
            start = i
            up = 0
            if AoI[i] != 0:
                up = AoI[i]
            #print("C")
            continue
        #ラストの時刻
        if i == len(AoI) - 1 and up == 0:
            end = i+1
            ans = ans + ((AoI[i])*(end - start)/2)
            if ((AoI[i])*(end - start)) % 2 == 1 :
                ans = ans + 0.5
            
        
        elif i == len(AoI) - 1 and up != 0:
            down = AoI[i]
            end = i+1
            ans = ans + ((up+down)*(end - start)/2)
            if ((up+down)*(end - start)) % 2 == 1 :
                ans = ans + 0.5
    return ans/move

def Time(AoI1,AoI2,AoI3,AoI4,AoI5,AoI6,AoI7,AoI8,AoI9,AoI10,AoI11,AoI12,AoI13,AoI14,AoI15,AoI16,AoI17,AoI18,AoI19,AoI20,route,position_info,i,j,AoI_coordinate,AoI_num,device_range):
    #デバイス１つずつにおいて,UAVの接近の判定と経過時間のリセットを行う.
    #現在地が指定の範囲内の場合,AoI値を0に再設定.半径2
    if i == -100:
        x = route[j][0]
        y = route[j][1]
    else:
        x = route[i][j][0]
        y = route[i][j][1]
        
    reach = [0]*len(AoI_coordinate)
    for k in range(len(AoI_coordinate)):
        rangeA = np.array([x,y])
        rangeB = np.array([AoI_coordinate[k][0],AoI_coordinate[k][1]])
        reach[k] = np.linalg.norm(rangeA - rangeB)

    #デバイス[5,-5]
    if reach[0] <= device_range:
        AoI1 = 0
    #デバイス[-5,3]
    if reach[1] <= device_range:
        AoI2 = AoI2 % AoI_num[1]
    #デバイス[6,6]
    if reach[2] <= device_range:
        AoI3 = AoI3 % AoI_num[2]
    #デバイス[-5,-4]
    if reach[3] <= device_range:
        AoI4 = AoI4 % AoI_num[3]
    #デバイス[-6,-1]
    if reach[4] <= device_range:
        AoI5 = AoI5 % AoI_num[4]
        
    #デバイス[6,-2]
    if reach[5] <= device_range:
        AoI6 = AoI6 % AoI_num[5]

    #デバイス[2,5]
    if reach[6] <= device_range:
        AoI7 = AoI7 % AoI_num[6]
    #デバイス[-1,2]
    if reach[7] <= device_range:
        AoI8 = AoI8 % AoI_num[7]
        
    #デバイス[5,2]
    if reach[8] <= device_range:
        AoI9 = AoI9 % AoI_num[8]
        
    #デバイス[-2,-3]
    if reach[9] <= device_range:
        AoI10 = AoI10 % AoI_num[9]
        
    #デバイス[-3,6]
    if reach[10] <= device_range:
        AoI11 = AoI11 % AoI_num[10]
        
    #デバイス[0,-6]
    if reach[11] <= device_range:
        AoI12 = AoI12 % AoI_num[11]
        
        
        
        
    if reach[12] <= device_range:
        AoI13 = AoI13 % AoI_num[12]
    #デバイス[-5,-4]
    if reach[13] <= device_range:
        AoI14 = AoI14 % AoI_num[13]
    #デバイス[-6,-1]
    if reach[14] <= device_range:
        AoI15 = AoI15 % AoI_num[14]
        
    #デバイス[6,-2]
    if reach[15] <= device_range:
        AoI16 = AoI16 % AoI_num[15]

    #デバイス[2,5]
    if reach[16] <= device_range:
        AoI17 = AoI17 % AoI_num[16]
    #デバイス[-1,2]
    if reach[17] <= device_range:
        AoI18 = AoI18 % AoI_num[17]
        
    #デバイス[5,2]
    if reach[18] <= device_range:
        AoI19 = AoI19 % AoI_num[18]
        
    #デバイス[-2,-3]
    if reach[19] <= device_range:
        AoI20 = AoI20 % AoI_num[19]
    
    
    return AoI1,AoI2,AoI3,AoI4,AoI5,AoI6,AoI7,AoI8,AoI9,AoI10,AoI11,AoI12,AoI13,AoI14,AoI15,AoI16,AoI17,AoI18,AoI19,AoI20

def nearest_DEVICE(UAV_coordinate,AoI_coordinate,AoI_num,device_num,device_range,route_direction,min_device,rank_device,num,mingroup_device_num,anothergroup_device_num,system_num,time,anothergroup_device_range,secset1,secset2):

    UAVkeep = 0
    #secset1 = int(len(AoI_coordinate)/2)
    #secset2 = int(len(AoI_coordinate) - len(AoI_coordinate)/2)
    for k in range(len(AoI_coordinate)):
        UAV = np.array([UAV_coordinate[0],UAV_coordinate[1]])
        DEVICE = np.array([AoI_coordinate[k][0],AoI_coordinate[k][1]])
        rank_device[k] = [np.linalg.norm(UAV - DEVICE),k+1]
        num[k] = np.linalg.norm(UAV - DEVICE)
        if num[k] <= device_range and device_num[k] != 1:
            device_num[k] = 1
            if k < secset1:
                mingroup_device_num[k] = 1
    num.sort()
    
    #systemnumが１の時のみ記録
    for i in range(len(anothergroup_device_num)):
        if time >= 30 and anothergroup_device_range[i] > rank_device[i+secset1][0] and system_num == 1:
            anothergroup_device_range[i] = rank_device[i+secset1][0]
            print("resetok")
            
    if  device_num == [1]*len(AoI_coordinate):
            device_num = [0]*len(AoI_coordinate)
            mingroup_device_num = [0]*secset1
            #anothergroup_device_num = [0]*(len(AoI_coordinate) - len(AoI_coordinate)/2)
            system_num = 1
            UAVkeep = 1
            
    return num,device_num,mingroup_device_num,anothergroup_device_num,rank_device,system_num,UAVkeep,anothergroup_device_range
    
def route_decide(UAV_coordinate,AoI_coordinate,AoI_num,device_num,device_range,route_direction,num_j,group,go_device,min_device_groupnumber,mingroup_device_num,anothergroup_device_num,system_num,time,AoI_start,keep_minroute_number,anothergroup_device_range,keep_UAV,go_device2,route_opt,go_sensa,keep_sensa,keep_AoI_coordinate):
    print("時刻")
    print(time)
    print("systemnum")
    print(system_num)
#まず情報更新可能なセンサ群を見つける　なければ最も更新頻度の高いセンサ方向へ向かわせる　(if分岐)
    #UAVがセンサとの通信半径内に存在した場合，センサに蓄積されたAoI値を低く更新する　(蓄積値/AoI更新頻度値)
    for i in range(len(AoI_coordinate)):
        UAV = np.array([AoI_coordinate[i][0],AoI_coordinate[i][1]])
        DEVICE = np.array([UAV_coordinate[0],UAV_coordinate[1]])
        XYreach = np.linalg.norm(UAV - DEVICE)
        if XYreach <= 2:
            device_num[i] = device_num[i] % AoI_num[i]
        
    #AoI値の更新　device_numに全てのセンサのAoI値を入力
    if time == 1:
        for i in range(len(AoI_coordinate)):
            device_num[i] = - AoI_start[i]
    else:
        
        for i in range(len(AoI_coordinate)):
            device_num[i] = device_num[i] + 1
        
        #情報更新可能なセンサを見つける
        #新しい巡回路構築用の配列
        AoIchange_num = []
        for i in range(len(AoI_coordinate)):
            if device_num[i] >= AoI_num[i] - 10:
                #情報更新可能なセンサの数を記録　問題なし
                AoIchange_num.extend([i+1])
        
        New_AoI_coordinate = [0]*len(AoIchange_num)
        New_num = 0
        for i in range(len(AoI_coordinate)):
            if device_num[i] >= AoI_num[i] - 10:
                #情報更新可能なセンサの座標を記録　問題なし
                New_AoI_coordinate[New_num] = copy.deepcopy(AoI_coordinate[i])
                New_num = New_num + 1
        
        if len(AoIchange_num) == 0:
        #いくべきセンサが無い場合には、最も更新頻度の高いセンサ方向へ向かわせる 問題なし
            min_num = 0
            min_start = 0
            for i in range(len(AoI_num)):
                if i == 0:
                    min_num = AoI_num[i]
                    min_start = AoI_start[i]
                    go = AoI_coordinate[i]
                elif min_num > AoI_num[i] and i != 0:
                    min_num = AoI_num[i]
                    min_start = AoI_start[i]
                    go = AoI_coordinate[i]
                elif min_num == AoI_num[i] and i != 0 and AoI_start[i] < min_start:
                    min_num = AoI_num[i]
                    min_start = AoI_start[i]
                    go = AoI_coordinate[i]
                #print(go)
            Xrange = go[0] - UAV_coordinate[0]
            Yrange = go[1] - UAV_coordinate[1]
            
        #UAVと目標デバイスまでのユークリッド距離を算出　問題なし
            UAV = np.array([go[0],go[1]])
            DEVICE = np.array([UAV_coordinate[0],UAV_coordinate[1]])
            XYreach = np.linalg.norm(UAV - DEVICE)
            print("XYreach1")
            print(XYreach)
        
            xnum = Xrange/XYreach
            ynum = Yrange/XYreach
            print("XとY")
            #xnumとynumはUAVの動きの変化量　この2つを出力することでUAVを動かす
            print(xnum)
            print(ynum)
            print("UAV")
            #UAVの座標
            print(UAV_coordinate)
            print(device_num)
            print(" ")
            
        
            return route_direction,go_device,device_num,mingroup_device_num,anothergroup_device_num,system_num,keep_UAV,go_device2,xnum,ynum,route_opt,go_sensa,keep_sensa,keep_AoI_coordinate
        
        else:
        #いくべきセンサが存在する場合，見つけたセンサ群を巡回するルートを設定
        #巡回路を作成 毎回作成していては失敗する．巡回路を作成するのは出発時のみ　巡回路を回り終えたのちに新たな巡回路を作成する．
            if system_num ==1:
                #keep_sensaの作成 これに巡回路に含まれるセンサの情報を保存
                #keep_sensa = [[[0,0],0,99]]*len(New_AoI_coordinate)
                keep_sensa = [[0,0,0] for i in range(len(New_AoI_coordinate))]
                t_num = 0
                for i in range(len(AoI_coordinate)):
                    if device_num[i] >= AoI_num[i] - 10:
                        print(AoI_coordinate[i])
                        print(keep_sensa)
                        keep_sensa[t_num][0] = AoI_coordinate[i]
                        #keep_sensa[t_num][1] = i
                        print(keep_sensa)
                        print(device_num)
                        t_num = t_num + 1
                      
                t_num = 0
                #センサの番号を入力
                for i in range(len(New_AoI_coordinate)):
                    keep_sensa[t_num][1] = i
                    t_num = t_num + 1
                    
                #2-opt での巡回路作成
                point_size = len(New_AoI_coordinate)
                distance_table = distance(New_AoI_coordinate)
                path_opt,route_opt = greedy0(New_AoI_coordinate)
                #print("途中結果")
                #print(route_opt)
                path_opt,total,route_opt = opt_2(point_size, path_opt,route_opt)
            
                print("UAV")
                #UAVの座標
                print(UAV_coordinate)
                #print(device_num)
                #print()
                print(" ")
                print("巡回路")
                print(route_opt)
                print(" ")
                
                keep_AoI_coordinate = [0]*len(AoIchange_num)
                New_num = 0
                for i in range(len(AoI_coordinate)):
                    if device_num[i] >= AoI_num[i] - 10:
                        #情報更新可能なセンサの数を記録及び更新　systemnum＝２の時に使用
                        keep_AoI_coordinate[New_num] =copy.deepcopy(AoI_coordinate[i])
                        New_num = New_num + 1
                
                #最初は巡回路に含まれる中で最も近いセンサへ行く route_optの番号と全センサの番号が噛み合っていない
                sensa_reach = [0,0]
                go_sensa = [0,0]
                print(New_AoI_coordinate)
                for i in range(len(New_AoI_coordinate)):
                    UAV = np.array([New_AoI_coordinate[i][0],New_AoI_coordinate[i][1]])
                    DEVICE = np.array([UAV_coordinate[0],UAV_coordinate[1]])
                    if i==0:
                        go_sensa = [route_opt[i]-1,np.linalg.norm(UAV - DEVICE)]
                        #print(go_sensa)
                    if i > 0 and go_sensa[1] > np.linalg.norm(UAV - DEVICE):
                        go_sensa = [route_opt[i]-1,np.linalg.norm(UAV - DEVICE)]
                        #print(go_sensa)
                #巡回路に従って動く
                print("目標センサ")
                print(go_sensa)
                print(New_AoI_coordinate[go_sensa[0]])
                print(New_AoI_coordinate)
                print(keep_sensa)
            
                #実際に扱いたいセンサ番号は１つずれている
                Xrange = New_AoI_coordinate[go_sensa[0]][0] - UAV_coordinate[0]
                Yrange = New_AoI_coordinate[go_sensa[0]][1] - UAV_coordinate[1]
            
                xnum = Xrange/go_sensa[1]
                ynum = Yrange/go_sensa[1]
                
                system_num = 2
                return route_direction,go_device,device_num,mingroup_device_num,anothergroup_device_num,system_num,keep_UAV,go_device2,xnum,ynum,route_opt,go_sensa,keep_sensa,keep_AoI_coordinate
                
            elif system_num == 2:
                #keep_sensaから
                sensa_change = 0
                #目標センサを通信範囲内に捉えた後は，次のセンサへ向かう
                UAV = np.array([keep_AoI_coordinate[go_sensa[0]][0],keep_AoI_coordinate[go_sensa[0]][1]])
                DEVICE = np.array([UAV_coordinate[0],UAV_coordinate[1]])
                XYreach = np.linalg.norm(UAV - DEVICE)
                if XYreach <= 2:
                    sensa_change = 1
                        
                #目標センサを次のセンサへ再設定する
                #巡回路に従って動くsystem_numを1に戻す条件 来訪したかどうかをkeep_sensaに記録. -9999が入る場所がずれてる
                for i in range(len(route_opt)):
                    if go_sensa[0] == keep_sensa[i][1] and sensa_change == 1:
                        print("成功")
                        print(keep_sensa)
                        print(i)
                        print(keep_sensa[i][1])
                        keep_sensa[i][2] = -9999
                        print(keep_sensa)
                        print(i)
                        print(keep_sensa[i][1])
                        
                #目標センサを次のセンサへ再設定する ここがうまく行っていないから一部のセンサに切り替わらない
                if sensa_change == 1:
                    for i in range(len(route_opt)):
                        if len(route_opt) == 1:
                        #センサが1つの時　ok
                            print("システム切り替え")
                            system_num = 1
                            break
                            
                        elif i == len(route_opt)-1:
                        #センサが2つ以上かつ、iがセンサ最大値と等しいとき
                            if go_sensa[0] == route_opt[i] - 1:
                                go_sensa[0] = route_opt[0] - 1
                                print("ここが原因？")
                                break
                                
                        else:
                            #センサが2つ以上かつ、i(目標デバイスの位置)がセンサ最大値を下回るとき ここに不具合
                            if go_sensa[0] == route_opt[i] - 1:
                                print("ここが原因2？")
                                print(go_sensa)
                                print(route_opt)
                                print(i)
                                go_sensa[0] = route_opt[i+1] - 1
                                print(go_sensa)
                                break
                        
                #巡回路の全てのセンサに来訪していればsystem_numを1に戻す
                #keep_sensaに記録された来訪記録が巡回路のセンサ数と等しくなれば，system_numを1に戻す ok
                end_count = 0
                for i in range(len(route_opt)):
                    if keep_sensa[i][2] == -9999:
                        end_count = end_count + 1
                if end_count == len(route_opt):
                    system_num = 1
                    print("システム切り替え")
                    
                print(" ")
                print("UAV")
                #UAVの座標
                print(UAV_coordinate)
                print(device_num)
                print(" ")
                print("確認")
                print(route_opt)
                print(go_sensa)
                print(keep_sensa)
                print(keep_AoI_coordinate)
                #UAVの移動方向の入力
                #実際に扱いたいセンサ番号は１つずれている
                
                UAV = np.array([keep_AoI_coordinate[go_sensa[0]][0],keep_AoI_coordinate[go_sensa[0]][1]])
                DEVICE = np.array([UAV_coordinate[0],UAV_coordinate[1]])
                reach = np.linalg.norm(UAV - DEVICE)
                
                Xrange = keep_AoI_coordinate[go_sensa[0]][0] - UAV_coordinate[0]
                Yrange = keep_AoI_coordinate[go_sensa[0]][1] - UAV_coordinate[1]
                xnum = Xrange/reach
                ynum = Yrange/reach
                
                
                return route_direction,go_device,device_num,mingroup_device_num,anothergroup_device_num,system_num,keep_UAV,go_device2,xnum,ynum,route_opt,go_sensa,keep_sensa,keep_AoI_coordinate
    
#次の時刻に向かうべき座標を指定　指定の方法は過去のユークリッド距離から座標指定する方法と同一
        
    #仮で置いているだけ
    xnum = 0
    ynum = 0
    print("失敗")
    print("　")
    print("　")
    return route_direction,go_device,device_num,mingroup_device_num,anothergroup_device_num,system_num,keep_UAV,go_device2,xnum,ynum,route_opt,go_sensa,keep_sensa,keep_AoI_coordinate
    
def point(num, pop_num,move,position_info,AoI_coordinate,AoI_num,device_range,group,min_device_number,AoI_start,keep_minroute_number,secset1,secset2):
    rangeXmax = 15
    rangeXmin = -15
    rangeYmax = 15
    rangeYmin = -15
    all_route = {}
    route_direction = {}
    device_num = [[0]]*len(AoI_coordinate)
    system_num = 1
    
    #計算前に一度数値入力.のちに数値を上書きする.
    #これを省くとエラーが発生してしまう.理由が理解しきれていないため解明と対策が必要.
    select_num = [rnd.randint(0, move) for _ in range(move)]
    all_route = [rnd.sample(select_num, move) for _ in range(pop_num)]
    all_route_keep = [rnd.sample(select_num, move) for _ in range(pop_num)]
    route_direction = [[[0,0]]*move]*pop_num
    
    Info_AoInum_device = [[0,0]]*len(AoI_coordinate)
    
    device_num = [0]*len(AoI_coordinate)

    mingroup_device_num = [0]*secset1
    anothergroup_device_num = [0]*secset2
    anothergroup_device_range = [999]*secset2
    keep_UAV = [0,0,0]
    
    route_opt = []
    go_sensa = []
    keep_sensa = []
    keep_AoI_coordinate = []
    min_device = 0
    go_device2 = 0
    #座標移動順を入力.
    for i in range(pop_num):
        for j in range(move):
            if j == 0 :
                all_route[i][j] = [0,0]
                all_route_keep[i][j] = [0,0]
                route_direction[i][j] = [0,0]
                continue
            x=0
            y=0
            [x,y] = all_route[i][j-1]
            
            #単位時間ごとの移動先の座標決定
            route_direction[i],min_device,device_num,mingroup_device_num,anothergroup_device_num,system_num,keep_UAV,go_device2,xnum,ynum,route_opt,go_sensa,keep_sensa,keep_AoI_coordinate = route_decide(all_route[i][j-1],AoI_coordinate,AoI_num,device_num,device_range,route_direction[i],j,group,min_device,min_device_number,mingroup_device_num,anothergroup_device_num,system_num,j,AoI_start,keep_minroute_number,anothergroup_device_range,keep_UAV,go_device2,route_opt,go_sensa,keep_sensa,keep_AoI_coordinate)
            
            x1 = xnum
            y1 = ynum
                #領域外へは移動させない
            if x+x1 > rangeXmax :
                x1=0
            if x+x1 < rangeXmin :
                x1=0
            if y+y1 > rangeYmax :
                y1=0
            if y+y1 < rangeYmin :
                y1=0
            x1 = x+x1
            y1 = y+y1
            all_route[i][j] = [x1,y1]
            all_route_keep[i][j] = [x1,y1]
            
    return all_route,all_route_keep

def generator(num, pop_num,move,AoI_coordinate,AoI_num,device_range,group,min_device_number,AoI_start,keep_minroute_number,secset1,secset2):
    #初めに座標情報を入力.
    y_coordinate =[15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12,-13,-14,-15]*31
    x_coordinate = []
    maxnum = 15
    maxmark = 31
    for i in range (maxmark):
        x_coordinate.extend([maxnum - i] * maxmark)
    coordinate = [[x_coordinate[i], y_coordinate[i]] for i in range(num)]

    position_info = {}
    for i in range(num):
        position_info[i] = coordinate[i]

    # 巡回順序生成
    all_route,all_route_keep = point(num, pop_num,move,position_info,AoI_coordinate,AoI_num,device_range,group,min_device_number,AoI_start,keep_minroute_number,secset1,secset2)
    
    return position_info, all_route,all_route_keep

def evaluate(position_info, all_route,all_route_keep,past_excellent,past_route,draw_past_index,pop_num,move,AoI_coordinate,AoI_num,device_range,AoI_start, loop):
    #評価の段階であり,単位時間あたりのAoI値と移動距離を導出
    #現在は合計AoI値が小さいものほど優良経路と評価しており,移動距離は評価基準に入れていない
    temp_evaluate_value = []
    evaluate_value = []
    reach_value = []
    graph_value = []
    graph = {}
    graph1 = {}
    graph2 = {}
    graph3 = {}
    graph4 = {}
    graph5 = {}
    graph6 = {}
    graph7 = {}
    graph8 = {}
    graph9 = {}
    graph10 = {}
    graph11= {}
    graph12 = {}
    graph_value = []
    graph1_value = []
    graph2_value = []
    graph3_value = []
    graph4_value = []
    graph5_value = []
    graph6_value = []
    graph7_value = []
    graph8_value = []
    graph9_value = []
    graph10_value = []
    graph11_value = []
    graph12_value = []
    graph13_value = []
    graph14_value = []
    graph15_value = []
    graph16_value = []
    graph17_value = []
    graph18_value = []
    graph19_value = []
    graph20_value = []
    answer = 0
    x_reach = 0
    y_reach = 0
    keep = 0
    keepnum = 0
    
    for i in range(pop_num):
            #合計AoI値でなく平均AoI値
        AoInum,graph1,graph2,graph3,graph4,graph5,graph6,graph7,graph8,graph9,graph10,graph11,graph12,graph13,graph14,graph15,graph16,graph17,graph18,graph19,graph20,graph1_value,graph2_value,graph3_value,graph4_value,graph5_value,graph6_value,graph7_value,graph8_value,graph9_value,graph10_value,graph11_value,graph12_value,graph13_value,graph14_value,graph15_value,graph16_value,graph17_value,graph18_value,graph19_value,graph20_value, = upAoI(all_route[i],move,AoI_coordinate,AoI_num,device_range,AoI_start)
        
        if i==0 :
            keep = AoInum
            keepnum = i
            keeproute = all_route[i]
            keepgraph1 = graph1
            keepgraph2 = graph2
            keepgraph3 = graph3
            keepgraph4 = graph4
            keepgraph5 = graph5
            keepgraph6 = graph6
            keepgraph7 = graph7
            keepgraph8 = graph8
            keepgraph9 = graph9
            keepgraph10 = graph10
            keepgraph11 = graph11
            keepgraph12 = graph12
            keepgraph13 = graph13
            keepgraph14 = graph14
            keepgraph15 = graph15
            keepgraph16 = graph16
            keepgraph17 = graph17
            keepgraph18 = graph18
            keepgraph19 = graph19
            keepgraph20 = graph20
        
        if AoInum < keep and i !=0:
            keep = AoInum
            keepnum = i
            keeproute = all_route[i]
            keepgraph1 = graph1
            keepgraph2 = graph2
            keepgraph3 = graph3
            keepgraph4 = graph4
            keepgraph5 = graph5
            keepgraph6 = graph6
            keepgraph7 = graph7
            keepgraph8 = graph8
            keepgraph9 = graph9
            keepgraph10 = graph10
            keepgraph11 = graph11
            keepgraph12 = graph12
            keepgraph13 = graph13
            keepgraph14 = graph14
            keepgraph15 = graph15
            keepgraph16 = graph16
            keepgraph17 = graph17
            keepgraph18 = graph18
            keepgraph19 = graph19
            keepgraph20 = graph20
        
        temp_evaluate_value.append(answer/20)
        evaluate_value.append(min(temp_evaluate_value))
        

        
    excellent_evaluate_value = keep
    route = keeproute
    draw_pop_index = keepnum
        
    return evaluate_value,excellent_evaluate_value,route,draw_pop_index,keepgraph1,keepgraph2,keepgraph3,keepgraph4,keepgraph5,keepgraph6,keepgraph7,keepgraph8,keepgraph9,keepgraph10,keepgraph11,keepgraph12,keepgraph13,keepgraph14,keepgraph15,keepgraph16,keepgraph17,keepgraph18,keepgraph19,keepgraph20,0

    
def show_route_reigai(position_info,route,AoI_coordinate,loop=0):
    #経路を描画を行う
    #最終地点から開始地点に移動線が引かれる不具合あり.修正予定
    num = 215
    num2 = len(route) - num
    x_coordinate = [route[num + i][0] for i in range(num2)]
    y_coordinate = [route[num + i][1] for i in range(num2)]
    
    #導出した最適路を描画
    plt.scatter(x_coordinate, y_coordinate)
    plt.plot(x_coordinate, y_coordinate)
    plt.ylim([-16,16])
    plt.xlim([-16,16])
    for i in range(len(AoI_coordinate)):
        #plt.scatter(0,0,c='r',s=100)
        plt.plot(AoI_coordinate[i][0],AoI_coordinate[i][1],'.',color='red',markersize = 15)
    plt.legend()
    plt.show()


def selection(all_route, evaluate_value, tournament_select_num, tournament_size, elite_select_num, ascending=False):
    """ トーナメント選択とエリート保存を行う"""

    select_pop = []
    elite_pop = []
    # トーナメント選択
    while True:
        select = rnd.sample(evaluate_value, tournament_size)
        select.sort(reverse=ascending)
        for i in range(tournament_select_num):
            value = select[i]
            index = evaluate_value.index(value)
            select_pop.append(all_route[index])

        # 個体数の半数個選択するまで実行
        if len(select_pop) >= len(all_route) / 2:
            break

    # エリート保存
    sort_evaluate_value = copy.deepcopy(evaluate_value)
    sort_evaluate_value.sort(reverse=ascending)
    for i in range(elite_select_num):
        value = sort_evaluate_value[i]
        index = evaluate_value.index(value)
        elite_pop.append(all_route[index])

    return select_pop, elite_pop

def show_graph(graph,movenum,num):
    #経路を描画を行う
    #最終地点から開始地点に移動線が引かれる不具合あり.修正予定

    y_coordinate = [graph[i] for i in range(move)]
    x_coordinate = [i for i in range(move)]
    
    value = AoInum(graph,move)

    print(" ")
    print(num)
    print(" ")
    print("平均AoI値")
    print(value)
    plt.plot(x_coordinate, y_coordinate)
    plt.plot(x_coordinate, y_coordinate)
    plt.ylim([0,700])
    plt.title("AoI値")
    plt.legend()
    plt.show()
    return value
    
def show_graph1(graph,move,generation_num):
    #経路を描画を行う
    #最終地点から開始地点に移動線が引かれる不具合あり.修正予定

    y_coordinate = [graph[i] for i in range(move)]
    x_coordinate = [i for i in range(move)]

    plt.plot(x_coordinate, y_coordinate)
    plt.plot(x_coordinate, y_coordinate)
    
    plt.ylim([0,700])
    plt.title("AoI値")
    plt.legend()
    plt.show()
    
def upAoI(route,move,AoI_coordinate,AoI_num,device_range,AoI_start):
    graph = {}
    graph1 = {}
    graph2 = {}
    graph3 = {}
    graph4 = {}
    graph5 = {}
    graph6 = {}
    graph7 = {}
    graph8 = {}
    graph9 = {}
    graph10 = {}
    graph11= {}
    graph12 = {}
    graph13 = {}
    graph14 = {}
    graph15 = {}
    graph16 = {}
    graph17 = {}
    graph18 = {}
    graph19 = {}
    graph20 = {}
    graph_value = []
    graph1_value = []
    graph2_value = []
    graph3_value = []
    graph4_value = []
    graph5_value = []
    graph6_value = []
    graph7_value = []
    graph8_value = []
    graph9_value = []
    graph10_value = []
    graph11_value = []
    graph12_value = []
    graph13_value = []
    graph14_value = []
    graph15_value = []
    graph16_value = []
    graph17_value = []
    graph18_value = []
    graph19_value = []
    graph20_value = []
    answer = 0
    keep = 0
    keepnum = 0
    AoI1 = 0
    AoI2 = 0
    AoI3 = 0
    AoI4 = 0
    AoI5 = 0
    AoI6 = 0
    AoI7 = 0
    AoI8 = 0
    AoI9 = 0
    AoI10 = 0
    AoI11 = 0
    AoI12 = 0
    AoI13 = 0
    AoI14 = 0
    AoI15 = 0
    AoI16 = 0
    AoI17 = 0
    AoI18 = 0
    AoI19 = 0
    AoI20 = 0
    
    
    for j in range(move):
        #単位時間あたりのAoI値を導出
        if AoI_start[0]< j :
            AoI1 = AoI1+1
        if AoI_start[1]< j :
            AoI2 = AoI2+1
        if AoI_start[2]< j :
            AoI3 = AoI3+1
        if AoI_start[3]< j :
            AoI4 = AoI4+1
        if AoI_start[4]< j :
            AoI5 = AoI5+1
        if AoI_start[5]< j :
            AoI6 = AoI6+1
        if AoI_start[6]< j :
            AoI7 = AoI7+1
        if AoI_start[7]< j :
            AoI8 = AoI8+1
        if AoI_start[8]< j :
            AoI9 = AoI9+1
        if AoI_start[9]< j :
            AoI10 = AoI10+1
        if AoI_start[10]< j :
            AoI11 = AoI11+1
        if AoI_start[11]< j :
            AoI12 = AoI12+1
        if AoI_start[12]< j :
            AoI13 = AoI13+1
        if AoI_start[13]< j :
            AoI14 = AoI14+1
        if AoI_start[14]< j :
            AoI15 = AoI15+1
        if AoI_start[15]< j :
            AoI16 = AoI16+1
        if AoI_start[16]< j :
            AoI17 = AoI17+1
        if AoI_start[17]< j :
            AoI18 = AoI18+1
        if AoI_start[18]< j :
            AoI19 = AoI19+1
        if AoI_start[19]< j :
            AoI20 = AoI20+1
        AoI1,AoI2,AoI3,AoI4,AoI5,AoI6,AoI7,AoI8,AoI9,AoI10,AoI11,AoI12,AoI13,AoI14,AoI15,AoI16,AoI17,AoI18,AoI19,AoI20 = Time(AoI1,AoI2,AoI3,AoI4,AoI5,AoI6,AoI7,AoI8,AoI9,AoI10,AoI11,AoI12,AoI13,AoI14,AoI15,AoI16,AoI17,AoI18,AoI19,AoI20,route,position_info,-100,j,AoI_coordinate,AoI_num,device_range)
            
        #合計AoI値でなく平均AoI値
        #三角形の面積の合計平均をとる.値のみを足すのではない.
        graph1[j] = AoI1
        graph2[j] = AoI2
        graph3[j] = AoI3
        graph4[j] = AoI4
        graph5[j] = AoI5
        graph6[j] = AoI6
        graph7[j] = AoI7
        graph8[j] = AoI8
        graph9[j] = AoI9
        graph10[j] = AoI10
        graph11[j] = AoI11
        graph12[j] = AoI12
        graph13[j] = AoI13
        graph14[j] = AoI14
        graph15[j] = AoI15
        graph16[j] = AoI16
        graph17[j] = AoI17
        graph18[j] = AoI18
        graph19[j] = AoI19
        graph20[j] = AoI20
            
    #平均AoI値を記録
    graph1_value = AoInum(graph1,move)
    graph2_value = AoInum(graph2,move)
    graph3_value = AoInum(graph3,move)
    graph4_value = AoInum(graph4,move)
    graph5_value = AoInum(graph5,move)
    graph6_value = AoInum(graph6,move)
    graph7_value = AoInum(graph7,move)
    graph8_value = AoInum(graph8,move)
    graph9_value = AoInum(graph9,move)
    graph10_value = AoInum(graph10,move)
    graph11_value = AoInum(graph11,move)
    graph12_value = AoInum(graph12,move)
    graph13_value = AoInum(graph13,move)
    graph14_value = AoInum(graph14,move)
    graph15_value = AoInum(graph15,move)
    graph16_value = AoInum(graph16,move)
    graph17_value = AoInum(graph17,move)
    graph18_value = AoInum(graph18,move)
    graph19_value = AoInum(graph19,move)
    graph20_value = AoInum(graph20,move)
    
    answer = graph1_value +graph2_value +graph3_value +graph4_value +graph5_value +graph6_value +graph7_value +graph8_value +graph9_value +graph10_value +graph11_value +graph12_value +graph13_value +graph14_value +graph15_value +graph16_value +graph17_value +graph18_value +graph19_value +graph20_value
    
    return answer/20,graph1,graph2,graph3,graph4,graph5,graph6,graph7,graph8,graph9,graph10,graph11,graph12,graph13,graph14,graph15,graph16,graph17,graph18,graph19,graph20,graph1_value,graph2_value,graph3_value,graph4_value,graph5_value,graph6_value,graph7_value,graph8_value,graph9_value,graph10_value,graph11_value,graph12_value,graph13_value,graph14_value,graph15_value,graph16_value,graph17_value,graph18_value,graph19_value,graph20_value
    
    
def show_route(position_info,route,AoI_coordinate,loop=0):
        #経路を描画を行う
    #最終地点から開始地点に移動線が引かれる不具合あり.修正予定

    routenum = 300
    x_coordinate = [route[i][0] for i in range(len(route))]
    y_coordinate = [route[i][1] for i in range(len(route))]
    #x_coordinate = [route[i+100][0] for i in range(routenum)]
    #y_coordinate = [route[i+100][1] for i in range(routenum)]
    
    #for i in range(int(len(route))):
    
    #導出した最適路を描画
    #点
    #plt.scatter(x_coordinate, y_coordinate)
    #線
    plt.plot(x_coordinate, y_coordinate)
    plt.ylim([-16,16])
    plt.xlim([-16,16])
    plt.xticks(fontsize = 16)
    plt.yticks(fontsize = 16)
    for i in range(len(AoI_coordinate)):
        #plt.scatter(0,0,c='r',s=100)
        #if i < min_device_num:
        if i < 12:
            plt.plot(AoI_coordinate[i][0],AoI_coordinate[i][1],'.',color='red',markersize = 15)
            #plt.plot(AoI_coordinate[i][0],AoI_coordinate[i][1],'^',color='blue',markersize = 15)
            #plt.plot(AoI_coordinate[i][0],AoI_coordinate[i][1],'^',color='lime',markersize = 8)
        else:
            #plt.plot(AoI_coordinate[i][0],AoI_coordinate[i][1],'.',color='red',markersize = 15)
            plt.plot(AoI_coordinate[i][0],AoI_coordinate[i][1],'^',color='black',markersize = 8)
            #plt.plot(AoI_coordinate[i][0],AoI_coordinate[i][1],'^',color='lime',markersize = 8)
            #plt.plot(AoI_coordinate[i][0],AoI_coordinate[i][1],'^',color='blue',markersize = 15)
    plt.legend()
    plt.show()
    

#120と140の間で一周目が終わっている　2021 12 7
# 115 215
# 初期生成時パラメータ
num = 961 #　地点数
pop_num = 1  # 個体数
generation_num = 1  # 世代数
move = 5000 #移動回数
loop = 0 # loop回数
device_range = 2 #通信半径


#デバイス20個を想定
#デバイス座標 複数パターン用意
#[1, 5, 9, 6, 7, 10, 8, 4, 2, 3]

#green ランダム配置１
AoI_coordinate = [[15,-15],[-15,15],[15,15],[-15,-15],[5,0],[0,13],[-6,5],[0,-5],[2,6],[-3,-4],[-6,10],[10,-2],[2,8],[-2,3], [4,1],[-3,-13],[-11,6],[1,-3],[-15,0],[-10,-5]]

#red ランダム配置2
#AoI_coordinate = [[-14,-12],[-14,12],[14,12],[14,-12],[7,6],[7,-6],[-7,6],[-7,-6],[14,0],[-14,0], [-14,6],[-14,-6],[-7,12],[-7,0], [-7,-12],[7,12],[7,0],[7,-12],[14,6],[14,-6]]

#情報更新頻度
AoI_num = [30,30,30,30,30,30,30,30,30,30,500,500,500,500,500,500,500,500,500,500]
#AoI_num = [50,50,100,100,150,150,200,200,250,250,300,300,350,350,400,400,450,450,500,500]
#情報生成開始時刻
#AoI_start = [0,10,20,30,40,50,60,70,80,90,100,120,140,150,180,200,220,240,275,300]
AoI_start = [0,70,80,90,100,110,120,130,140,150,160,170,180,190,200,210,220,230,240,250]

#小さいデバイスの数
min_device_num = 20
j_num = min_device_num

secset1 = min_device_num
secset2 = int(len(AoI_coordinate) - min_device_num)

#group[0]にデバイス番号,group[1]にグループ番号を入力
group = [[0,0,0,0]]*len(AoI_coordinate)
min_device_number = [0]*secset1
group,min_device_number = device_search(AoI_coordinate,AoI_num,group,secset1,secset2)
     
#20220629 20の時 改良
keep_minroute_number = [1, 12, 5, 15, 18, 8, 16, 4, 20, 19, 17, 2, 11, 7, 10, 14, 9, 13, 6, 3]
true_minroute_number = [1, 12, 5, 15, 18, 8, 16, 4, 20, 19, 17, 2, 11, 7, 10, 14, 9, 13, 6, 3]


#座標入力
position_info, all_route,all_route_keep = generator(num, pop_num,move,AoI_coordinate,AoI_num,device_range,group,min_device_number,AoI_start,keep_minroute_number,secset1,secset2)
print("ok")
# 評価
excellent_evaluate_value = []
route = []
draw_pop_index = []

route_keep = []
route_keep = [[[move]*2]*move]*(generation_num+1)
graph_value = []
graph1_value = []
graph2_value = []
graph3_value = []
graph4_value = []
graph5_value = []
graph6_value = []
graph7_value = []
graph8_value = []
graph9_value = []
graph10_value = []
graph11_value = []
graph12_value = []
graph13_value = []
graph14_value = []
graph15_value = []
graph16_value = []
graph17_value = []
graph18_value = []
graph19_value = []
graph20_value = []
Allgraph_value = {}

bestgraph = {}
bestgraph_num = 0
bestgraph_AoI = {}

routenum = {}
graph = {}
graph1 = {}
graph2 = {}
graph3 = {}
graph4 = {}
graph5 = {}
graph6 = {}
graph7 = {}
graph8 = {}
graph9 = {}
graph10 = {}
graph11 = {}
graph12 = {}
graph13 = {}
graph14 = {}
graph15 = {}
graph16 = {}
graph17 = {}
graph18 = {}
graph19 = {}
graph20 = {}
number = 0
sift_number = 0

# 評価
evaluate_value,excellent_evaluate_value,route,draw_pop_index,graph1,graph2,graph3,graph4,graph5,graph6,graph7,graph8,graph9,graph10,graph11,graph12,graph13,graph14,graph15,graph16,graph17,graph18,graph19,graph20,number = evaluate(position_info,all_route,all_route_keep,excellent_evaluate_value,route,draw_pop_index,pop_num,move,AoI_coordinate,AoI_num,device_range,AoI_start, loop)


print("導出経路")
show_route(position_info,route,AoI_coordinate,loop)
#show_route_reigai(position_info,route,AoI_coordinate,loop)

num1 = show_graph(graph1,move,1)
num2 = show_graph(graph2,move,2)
num3 = show_graph(graph3,move,3)
num4 = show_graph(graph4,move,4)
num5 = show_graph(graph5,move,5)
num6 = show_graph(graph6,move,6)
num7 = show_graph(graph7,move,7)
num8 = show_graph(graph8,move,8)
num9 = show_graph(graph9,move,9)
num10 = show_graph(graph10,move,10)
num11 = show_graph(graph11,move,11)
num12 = show_graph(graph12,move,12)
num13 = show_graph(graph13,move,13)
num14 = show_graph(graph14,move,14)
num15 = show_graph(graph15,move,15)
num16 = show_graph(graph16,move,16)
num17 = show_graph(graph17,move,17)
num18 = show_graph(graph18,move,18)
num19 = show_graph(graph19,move,19)
num20 = show_graph(graph20,move,20)

numall = num1 + num2 + num3 + num4 + num5 + num6 + num7 + num8 + num9 + num10 + num11 + num12 + num13 + num14 + num15 + num16 + num17 + num18 + num19 + num20

numall = numall / 20
print("平均AoI")
print(numall)
print("導出経路")

show_route(position_info,route,AoI_coordinate,loop)

num1 = show_graph(graph1,move,1)
num2 = show_graph(graph2,move,2)
num3 = show_graph(graph3,move,3)
num4 = show_graph(graph4,move,4)
num5 = show_graph(graph5,move,5)
num6 = show_graph(graph6,move,6)
num7 = show_graph(graph7,move,7)
num8 = show_graph(graph8,move,8)
num9 = show_graph(graph9,move,9)
num10 = show_graph(graph10,move,10)
num11 = show_graph(graph11,move,11)
num12 = show_graph(graph12,move,12)
num13 = show_graph(graph13,move,13)
num14 = show_graph(graph14,move,14)
num15 = show_graph(graph15,move,15)
num16 = show_graph(graph16,move,16)
num17 = show_graph(graph17,move,17)
num18 = show_graph(graph18,move,18)
num19 = show_graph(graph19,move,19)
num20 = show_graph(graph20,move,20)
print("平均AoI")
print(numall)
print("導出経路")
show_route(position_info,route,AoI_coordinate,min_device_num,loop)

